﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public class MongoRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        private readonly IMongoCollection<T> _dataCollection;

        public MongoRepository(IDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);
            _dataCollection = database.GetCollection<T>(typeof(T).Name);
        }
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var result = await _dataCollection.FindAsync(employee => true);
            return result.ToList();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            var result = await _dataCollection.FindAsync(employee => employee.Id == id);
            return result.Single();
        }

        public async Task AddAsync(T newEmployee)
        {
            await _dataCollection.InsertOneAsync(newEmployee);
        }

        public async Task UpdateAsync(T updatedEmployee)
        {
            await _dataCollection.ReplaceOneAsync(employee => employee.Id == updatedEmployee.Id, updatedEmployee);
        }

        public async Task DeleteAsync(T toDelete)
        {
            await _dataCollection.DeleteOneAsync(employee => employee.Id == toDelete.Id);
        }

        public Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
        {
            throw new NotImplementedException();
        }

    }
}
