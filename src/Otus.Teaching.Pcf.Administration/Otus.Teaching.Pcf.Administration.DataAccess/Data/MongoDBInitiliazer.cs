﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class MongoDBInitiliazer
        : IDbInitializer
    {
        private readonly IMongoDatabase _database;
        private readonly IDatabaseSettings _settings;
        public MongoDBInitiliazer(IDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            _database = client.GetDatabase(settings.DatabaseName);
            _settings = settings;
        }
        
        public void InitializeDb()
        {
            _database.DropCollection(typeof(Role).Name);
            _database.DropCollection(typeof(Employee).Name);
          
            _database.CreateCollection(typeof(Role).Name);
            _database.CreateCollection(typeof(Employee).Name);
            _database.GetCollection<Employee>(typeof(Employee).Name).InsertMany(FakeDataFactory.Employees);
            _database.GetCollection<Role>(typeof(Role).Name).InsertMany(FakeDataFactory.Roles);
        }
    }
}